# Challenge VTEX-IO #

## Sobre o desafio ##

Nosso principal objetivo será garantir as melhores práticas a serem adotadas em nossas squads por todos desenvolvedores, além de criar e suportar softwares digitais que tragam visibilidade para nossa operação.
 
Esse desafio, irá avaliar sua capacidade de implementar `blocks` `interfaces` e `components` VTEX IO na construção de, pelo menos, um módulo (ex: vitrine de produtos, header e footer).
 
Fique a vontade para implementar todo seu conhecimento em VTEX IO, quanto mais complexo for sua entrega, maior será sua pontuação.
O desafio é separado em **3 níveis de complexidade**, você pode escolher um ou mais níveis no qual deseja desenvolver.
	
**Nível 01**
Caso opte por desenvolver com app nativos VTEX use como base a loja demo da VTEX <https://storetheme.vtex.com>.

**Nível 02**
Se preferir construir components em ReactJS disponibilizamos um layout que pode ser utilizado como base **(Não é obrigatório seguir esse layout)**:
<https://www.figma.com/file/O9AEeYB6ZWyMTZzMZhvjaY/loja-vtex-jussi?node-id=0%3A1>.

**Nível 03**
Consuma alguma API publica de seu conhecimento, utilizando o `node` e `graphql`, se preferir, utilize o masterdata como base de dados para um CRUD. **É necessário a visualização dos dados no front-end.**

Esse APP do VTEX IO suporta os seguintes builders:

	- admin
	- react
	- node
	- pixel
	- graphql
	- configuration
	- docs
	- messages
	- store

**Observações IMPORTANTES**
Não serão consideradas configurações realizadas pelo `site-editor`, o importante do desafio é a implementação de código fonte e boas práticas.

**Documentação de Apoio:**

<https://vtex.io/docs/getting-started/what-is-vtex-io/1/>

<https://developers.vtex.com/>

-----
	
## Siga as instruções abaixo	

### Passo 01 : Ambiente ###

1.0 - Faça o importe deste repositório em seu ambiente de desenvolvimento (github ou bitbucket) usando a seguinte url <https://bitbucket.org/jussilabs/jussilabs-challenge-vtex-io.git>

1.1 - Libere acesso ao seguinte usuário <jussilabs-admin@jussi.com.br> em seu repositório.

1.2 - Crie uma `branch` para realizar o desenvolvimento e faça todos os `commits` nela. (Ex: `challenge-dev`)

1.3 - Faça o login na loja <https://jussilabs.myvtex.com/> utilizando o seguinte usuário <jussilabs-challenge@jussi.com.br> solicite o envio do código por email. Você receberá o código no seu email pessoal informado para cadastro anteriormente.

 * O processo de login é realizado via `VTEX toobelt` na account `jussilabs` e ao acessar a `workspace` pela primeira vez no navegador.

1.5 - Crie um `workspace` com seu nome + sobrenome (Ex : `joaosilva`)

1.6 - Link a `branch` que criou em sua `workspace`

1.7 - Acesse a URL no navegador o resultado final deve ser esse:

![](https://jussilabs.vteximg.com.br/arquivos/welcome_jussilabs_CHALLENGE_VTEX_IO.png)

----

### Passo 02 : desenvolvimento ###

2.0 - Faça todo o desenvolvimento desejado e sempre commit em sua `branch` criada no passo anterior.

2.1 - Mantenha sempre sua workspace VTEX IO com o `build` (link) da última versão que você desenvolveu.

2.2 - Ao concluir, abra um `pull-request` para a `branch` `master` e inclua o usuário <jussilabs-admin@jussi.com.br> como revisor. O `pull-request` deve conter as seguintes informações:

-Url da `workspace`

-Resumo do que foi desenvolvido (Ex: Component de galeria de fotos que realiza `get` nas fotos na API do <https://giphy.com/>)

-Caso precise rodar algum comando extra de dependências, detalhar.

2.3 - Envie um email para <jussilabs-admin@jussi.com.br> informando que você finalizou o teste.


### Passo 03 : Avaliação ###

3.0 - A avaliação será realizada no `pull-request` aberto.

3.1 - Este desafio será avaliado pela equipe do jussilabs.


----

Se você estiver tendo problemas com o desafio, sinta-se à vontade para entrar em contato conosco pelo email <jussilabs-admin@jussi.com.br>

----

# BOA SORTE :)
